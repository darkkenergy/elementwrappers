/**
 * @name ElementWrappers
 * @desc A basic wrapper module for some common HTMLElement Objects, in order to
 *      ease stubbing of those Objects in unit tests, by use of something like
 *      proxyquire (https://github.com/thlorenz/proxyquire.)
 * 
 * @api Element {function}: Returns the Element interface.
 * @api HTMLElement {function}: Returns the HTMLElement interface.
 * @api HTMLFormElement {function}: Returns the HTMLFormElement interface.
 * @api HTMLInputElement {function}: Returns the HTMLInputElement interface.
 */
module.exports = {
    Element: function() {
        return Element;
    },
    HTMLElement: function() {
        return HTMLElement;
    },
    HTMLFormElement: function() {
        return HTMLFormElement;
    },
    HTMLInputElement: function() {
        return HTMLInputElement;
    }
};